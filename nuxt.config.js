module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Momentum',
    meta: [
      { charset: 'utf-8' },
      { name: 'theme-color', content: '#343a40'},
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Achieve your fitness goals' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    splitChunks: {
      layouts: true
    },
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.test\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    // html:{ minify:{
    //   minifyJS: false,
    // }}
  },
  modules: ['bootstrap-vue/nuxt' ],
  env:{
    API_ENDPOINT : "https://api.momentvm.co.uk",//"http://api.localhost.com:3003",//
    AUTH_ENDPOINT:  "https://auth.momentvm.co.uk",//"http://auth.localhost.com:3000/dev",//
  },
  plugins: ['~/plugins/vue-inject.js'],
  // server: {
  //   host: "127.0.0.1",
  //   port: 3000
  // }
}

