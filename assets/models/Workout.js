import BaseModel from "./BaseModel";
import Rest from "../../middleware/Rest";
export default class Workout {
  id;
  created_at;
  exercises = [];
  status;
  constructor(args = {}) {
    this.id = args["id"];
    this.created_at = args["created_at"];
    this.status = args["status"];
    this.exercises = Array.isArray(args["exercises"]) ? args["exercises"] : [];
  }

  _metadata() {
    return {
      endpoint: "workouts/",
      currentWorkoutEndpoint: "workouts/current"
    };
  }

  async save(restService) {
    var endpoint = this._metadata().endpoint;
    var data = JSON.stringify(this);
    var result = {};

    var options = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    let response = await restService.post(endpoint, data, options);
    return response.data;
  }

  static currentWorkoutEndpoint = "workouts/current";
  static finishWorkoutEndpoint = "workouts/current/finish";
  static getWorkoutEndpoint(id) {
    return `workouts/${id}`;
  }

  static workoutsEndpoint = "workouts/";
  static async getAll(restService) {
    if (!restService) return;
    let endpoint = this.workoutsEndpoint;
    let response = (await restService.getAllClass(endpoint, this)) || {};
    let workouts = response.data || [];

    return workouts;
  }

  static async getCurrent(restService) {
    if (!restService) return;
    var response = await restService.get(this.currentWorkoutEndpoint);
    var workout = new Workout(response.data);
    return workout;
  }

  static async get(restService, id) {
    if (!restService || !id) return;
    var response = await restService.get(this.getWorkoutEndpoint(id));
    var workout = new Workout(response.data);
    return workout;
  }

  static async finishCurrent(restService) {
    if (!restService) return;
    let result = await restService.post(this.finishWorkoutEndpoint);
    return;
  }
}
