import axios from "axios";
axios.defaults.withCredentials = true;
export default class BaseModel {
  constructor(){
    this.API_ENDPOINT=process.env.API_ENDPOINT
  }

  static _API_ENDPOINT(){
    return process.env.API_ENDPOINT
  }

  post = async (endpoint, data = {}, options = {}) => {
    let result;
    await axios.post(
      `${this.API_ENDPOINT}/${endpoint}`,
      data,
      options).then(response => {
      result = response.data;
    });

    return result;
  }
  getAll = async (endpoint) => {
    var results;

    await axios.get(`${this.API_ENDPOINT}/${endpoint}`).then(response => {
      results = response.data;
    }).catch(error => {
      results = [];
    });

    return results;
  }



  
}