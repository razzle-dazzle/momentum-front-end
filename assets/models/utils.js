export function compare(key) {
  return (a, b) => {
    const val1=a[key]?.toLowerCase(),
          val2=b[key]?.toLowerCase();
    if (val1 < val2)
        return -1 
    if (val1 > val2)
        return 1
    return 0
  }
}