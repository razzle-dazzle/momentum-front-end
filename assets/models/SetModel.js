export default class Set {
  constructor(args = {}) {
    this.weight = args["weight"] ? args["weight"] : undefined;
    this.reps = args["reps"] ? args["reps"] : undefined;
  }
}
