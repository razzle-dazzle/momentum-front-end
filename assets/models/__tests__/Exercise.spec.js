import "babel-polyfill";
import Exercise from "../Exercise";
// let Model = require('../Exercise')

describe("Exercise Model", () => {
  test("Initialise Exercise", async () => {
    let exercise = new Exercise();
    expect(Object.keys(exercise)).toEqual(['id',"name",'created_at']);
  });
  test("Get Exercise", async () => {
    let exercise = await Exercise.get(restService, "id-123");
    expect(exercise instanceof Exercise).toBe(true);
  });
  test("Get All Exercises", async () => {
    let exercise = await Exercise.getAll(restService);
    expect(exercise[0] instanceof Exercise).toBe(true);
  });
  test("Save Exercise", async () => {
    let exercise = new Exercise({ name: "Test" });
    let response = await exercise.save(restService);
    expect(response).toHaveProperty(["name"]);
  });
  test("Get Exercise No Rest Service", async () => {
    let response = await Exercise.get();
    expect(response).toBeFalsy();
  });
  test("Get All Exercises No Rest Service", async () => {
    let exercise = await Exercise.getAll();
    expect(exercise).toBeFalsy();
  });
});

let data = [
  {
    id: "123",
    name: "Pushups",
    created_at: "2020-06-06T14:40:20.00000Z"
  },
  {
    id: "124",
    name: "Situps",
    created_at: "2020-06-06T15:40:20.00000Z"
  }
];

let restService = {
  get() {
    return new Promise((resolve, reject) => resolve({ data: data[0] }));
  },
  getAll() {
    return new Promise((resolve, reject) => resolve({ data: data }));
  },
  post(endpoint,data) {
    return new Promise((resolve, reject) => resolve({ data: JSON.parse(data) }));
  }
};
