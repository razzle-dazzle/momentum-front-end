import { compare } from "./utils";

export default class ExerciseEntry {
  id;
  created_at;
  sets = [];
  exercise_id;
  exercise_name;
  constructor(args= {}) {
    this.id = args["id"];
    this.created_at = args["created_at"];
    this.exercise_id = args["exercise_id"] || 0;
    this.sets = Array.isArray(args["sets"]) ? args["sets"] : [];
  }

  endpoints(){
    return {
      createExerciseEntryEndpoint(workout_id){
        return `workouts/${workout_id}/exercises/`
      },
      updateExerciseEntryEndpoint(workout_id, exercise_entry_id){
        return `workouts/${workout_id}/exercises/${exercise_entry_id}`
      }
    }
  }

  async create(restService,workout_id) {
    if (!restService)
    return
    const endpoint = this.endpoints().createExerciseEntryEndpoint(workout_id);
    // Will need changing
    var data = JSON.stringify(Object.assign({},{workout_id },this))
    // var data = JSON.stringify(this);
    var result;

    const options = {
        headers :  {
          'Content-Type': 'application/json',
        }
      }
      
    result = await restService.post(endpoint, data, options);
    Object.assign(this,result.data)
    
    return result.data;
  }

  async update(restService,workout_id) {
    if (!restService)
    return
    const endpoint = this.endpoints().updateExerciseEntryEndpoint(workout_id,this.id);
    var data = JSON.stringify({ sets: this.sets});
    var result;

    const options = {
        headers :  {
          'Content-Type': 'application/json',
        }
      }
      
    result = await restService.patch(endpoint, data, options);
    
    return result.data;
  }


  static getWorkoutExerciseEntriesEndpoint(workout_id) {
    return `workouts/${workout_id}/exercises/`
  }

  static async getAll(restService, workout_id){
    if (!restService)
    return
    let endpoint = this.getWorkoutExerciseEntriesEndpoint(workout_id);
    let response = await restService.getAllClass(endpoint, this) || {}

    var exercise_entries = response.data  || [];
    exercise_entries.sort(compare('created_at'));

    return exercise_entries;
  }
}
