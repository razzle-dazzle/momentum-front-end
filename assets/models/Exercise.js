import { compare } from './utils';

export default class Exercise {
  // TODO: Change to TS
  // id: string;
  // name: string;
  // created_at: string;
  constructor(args = {}) {
    this.id = args["id"];
    this.name = args["name"];
    this.created_at = args["created_at"];
  }

  _metadata (){
    return {
      endpoint: 'exercises/'
    };
  };
  
  async save(restService) {
    var endpoint = this._metadata().endpoint;
    var data = JSON.stringify(this);
    
    var options = {
      headers :  {
        'Content-Type': 'application/json',
      }
    }
    
    let response  = await restService.post(endpoint, data, options);
    return response.data;
  }


  static getExercisesEndpoint() {
    return `exercises/`
  }
  
  static async getAll(restService){
    if (!restService)
    return
    let response = await restService.getAll(this.getExercisesEndpoint());
    var exercises = [];
    
    response.data.map((exercise)=>{
      exercises.push(new Exercise(exercise))
    })

    exercises.sort(compare('name'));
    return exercises;
  }
  
  static getExerciseEndpoint(id) {
    return `exercises/${id}`
  }

  static async get(restService, id){
    if (!restService || !id)
      return
    var response = await restService.get(this.getExerciseEndpoint(id));
    var exercise = new Exercise(response.data)
    return exercise;
  }
}
