// import axios from "axios";
// axios.defaults.withCredentials = true
import Rest from "./Rest";
export default class JWTRest {
  constructor(endpoint) {
    if (endpoint) this.API_ENDPOINT = endpoint;
    else this.API_ENDPOINT = process.env.API_ENDPOINT;
    this.AUTH_ENDPOINT = process.env.AUTH_ENDPOINT;

    this.rest = new Rest(this.API_ENDPOINT);
    this.auth = new Rest(this.AUTH_ENDPOINT);
  }

  refreshToken = async function() {
    return this.auth.get("refreshtoken");
  };

  authWrapper(call, args) {
    return new Promise(async (resolve, reject) => {
      try {
        // Tries an API call
        let result = await call(...args);
        resolve(result);
      } catch (err) {
        // Response in case token cannot be refreshed
        let initialErrorResponse = err;
        if (err.status == 401) {
          try {
            // If token expired
            // Tries to refresh the token and make the API call
            await this.refreshToken();
            let result = await call(...args);
            resolve(result);
          } catch (err) {
            reject(initialErrorResponse);
          }
        } else {
          reject(err);
        }
      }
    });
  }

  post = async (...args) => {
    return this.authWrapper(this.rest.post, args);
  };

  patch = async (...args) => {
    return this.authWrapper(this.rest.patch, args);
  };

  getAll = async (...args) => {
    return this.authWrapper(this.rest.getAll, args);
  };
  get = async (...args) => {
    return this.authWrapper(this.rest.get, args);
  };
  getAllClass = async (...args) => {
    return this.authWrapper(this.rest.getAllClass, args);
  };
}
