import axios from "axios";
// axios.defaults.withCredentials = true;

var axiosRest = axios.create({
  withCredentials: true
})
export default class Rest {
  constructor(baseUrl) {
    if (baseUrl) 
      this.API_ENDPOINT = baseUrl;
    else 
      this.API_ENDPOINT = process.env.API_ENDPOINT;
  }

  post = async (endpoint, data = {}, options = {}, fullResponse) => {
    
    return new Promise((resolve, reject) => {
      axiosRest
        .post(`${this.API_ENDPOINT}/${endpoint}`, data, options)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error.response);
        });
    });
    
  };
  patch = async (endpoint, data = {}, options = {}) => {
    return new Promise((resolve, reject) => {
      axiosRest
        .patch(`${this.API_ENDPOINT}/${endpoint}`, data, options)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error.response);
        });
    });

  };
  getAll = async endpoint => {
    return new Promise((resolve, reject) => {
      axiosRest
        .get(`${this.API_ENDPOINT}/${endpoint}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error.response);
        });
    });

  };
  get = async endpoint => {
    return new Promise((resolve, reject) => {
      axiosRest
        .get(`${this.API_ENDPOINT}/${endpoint}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error.response);
        });
    });
  };
  getAllClass = async (endpoint, cls) => {
    return new Promise((resolve, reject) => {
      axiosRest
        .get(`${this.API_ENDPOINT}/${endpoint}`)
        .then(response => {
          let results= [];
          let data = response.data || [];
          data.map(item => {
            results.push(new cls(item));
          });
          response.data = results;
          resolve(response);
        })
        .catch(error => {
          reject(error.response);
        });
    });
  };
}