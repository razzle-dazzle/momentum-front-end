# FROM node:12.2.0-alpine
FROM node:alpine3.10
RUN mkdir app

WORKDIR /app

# COPY . /app

RUN npm install -g @vue/cli
RUN npm install -g serve

ENTRYPOINT npm run dev

ENV HOST 0.0.0.0
EXPOSE 3000
