import { shallowMount } from "@vue/test-utils";
const commonValidations = require("../common");

describe("It assesses the common validations", () => {
  test("Validates UUID as true", () => {
    expect(commonValidations.isUuid("123abc-23dkjasw")).toBe(true);
  });
  test("Validates UUID as false", () => {
    expect(commonValidations.isUuid("123abc-$23dkjasw")).toBe(false);
  });
  test("Validates UUID as false", () => {
    expect(commonValidations.isUuid()).toBe(false);
  });
});
