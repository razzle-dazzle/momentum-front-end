module.exports = {
  isUuid(value = "") {
    return /^[A-Za-z0-9\-]+$/g.test(value);
  },
  /**
   * Returns the sum of a and b
   * @param {String} password
   * @returns {Array} Array of errors if any. Defaults to []
   */
  validatePassword(password="") {
    let errors = [];
    if(password == "")
      return ['Please provide a password.']
    let passwordChecks = {
      hasUppercase: {
        value: false,
        message: "Password must contain at least one upper case character"
      },
      hasLowercase: {
        value: false,
        message: "Password must contain at least one lower case character"
      },
      hasSpecialCharacter: {
        value: false,
        message:
          "Password must contain at least one special character \ ! @ [ ] # $ % ^ & * . _ ,"
      }
    };

    if (password.length < 10)
      errors.push("Password must have at least 10 characters");

    for (var i = 0; i < password.length; i++) {
      let currentChar = password.charAt(i);
      if (/[\\!@\[\]#\$%\^\&\*\.\_\,]/g.test(currentChar)) {
        passwordChecks.hasSpecialCharacter.value = true;
        continue;
      }
      if (currentChar.toLowerCase() == currentChar) {
        passwordChecks.hasLowercase.value = true;
        continue;
      }
      if (currentChar.toUpperCase() == currentChar) {
        passwordChecks.hasUppercase.value = true;
        continue;
      }
    }

    for (let key in passwordChecks) {
      if (passwordChecks.hasOwnProperty(key))
        if (passwordChecks[key].value == false)
          errors.push(passwordChecks[key].message);
    }

    return errors;
  }
};