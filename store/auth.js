import Rest from "../middleware/Rest";
import JWTRest from "../middleware/JWTRest";
import { validatePassword } from "../utils/validation/common.js";
export const state = () => ({});

export const actions = {
  async logout() {
    let rest = new Rest(process.env.AUTH_ENDPOINT);
    return new Promise(async (resolve, reject) => {
      await rest
        .get("logout")
        .then(res => {
          if (res.status === 200) {
            resolve(true);
          }
        })
        .catch(err => {
          resolve(false);
        });
    });
  },
  async verifyAccount(dispatch, { token, userId }) {
    let rest = new Rest(process.env.AUTH_ENDPOINT);
    let endpoint = `register/verify?token=${token}&userId=${userId}`;
    return new Promise(async (resolve, reject) => {
      await rest
        .get(endpoint)
        .then(res => {
          if (res.status === 204) {
            resolve(true);
          }
        })
        .catch(err => {
          reject(false);
        });
    });
  },
  async forgotPassword(dispatch, { email }) {
    let rest = new Rest(process.env.AUTH_ENDPOINT);
    let endpoint = `accounts/forgot-passsword`;
    return new Promise(async (resolve, reject) => {
      rest
        .post(endpoint, { email })
        .then(res => {
          if (res.status === 202) {
            resolve(true);
          } else reject("Something went wrong.");
        })
        .catch(err => {
          reject("Something went wrong.");
        });
    });
  },
  async resetPassword(dispatch, { token, userId, password }) {
    let rest = new Rest(process.env.AUTH_ENDPOINT);
    let errors = [];
    let endpoint = `accounts/reset-password`;

    return new Promise(async (resolve, reject) => {
      errors = validatePassword(password);
      if (errors.length) {
        reject(errors);
        return;
      }
      rest
        .post(endpoint, { token, userId, password })
        .then(res => {
          if (res.status === 204) {
            resolve(true);
          } else reject("Something went wrong.");
        })
        .catch((err) => {
          reject("Something went wrong.");
        });
    });
  },
  async isLoggedIn(dispatch) {
    let rest = new JWTRest();
    return new Promise(async (resolve, reject) => {
      rest.refreshToken()
        .then((data)=>{
          resolve(true)
        }
        )
        .catch((err)=>{
          resolve(false)
        })
    });
  }
};
