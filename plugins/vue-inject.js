import Vue from 'vue';
import axios from 'axios';
import { BootstrapVueIcons } from 'bootstrap-vue'
Vue.use(require('vue-moment'))
Vue.use(BootstrapVueIcons)
Vue.prototype.$axios = axios;
Vue.prototype.$myInjectedFunction = string => console.log('This is an example', string)